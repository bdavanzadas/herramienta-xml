/**
 * 
 */
package dominio;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * @author Sergio Fernández
 *
 */
public class Extractor {

	
	//Medida de los tiempos
	static long time_start = 0;
	static long time_end = 0;


	private static void startTimeCount() {
		time_start = System.currentTimeMillis();
	}

	private static long stopTimeCount() {
		time_end = System.currentTimeMillis();
		return time_end - time_start;
	}

	/**
	 * @param args
	 */
	public Extractor() {
	}

	public void extraerCategoriaDe(String categoria, String fichero_xml) {

		// /Inicializacion de variables de acceso a fichero
		File ini;
		FileReader fr;
		BufferedReader br;

		FileWriter fw = null;		
		PrintWriter pw = null;

		//Axuliares
		String aux = "";
		String post="";
		String mainccat = categoria;

		boolean almacena = false;
		boolean hilo_encontrado = false;
		long contador = 0; 


		// Acceso a fichero
		try {
			ini = new File(fichero_xml);
			fr = new FileReader(ini);
			br = new BufferedReader(fr);
			Scanner fichero = new Scanner(br);

			fw = new FileWriter(mainccat+".xml");
			pw = new PrintWriter(fw);

			startTimeCount();

			pw.println("<?xml version='1.0' encoding='UTF-8'?>\n<feeds>");
			while (fichero.hasNextLine()) {
				aux = fichero.nextLine();
				post+=aux+"\n";
				if(aux.startsWith("<vespaadd>")){
					aux = aux.replaceAll("<vespaadd>", "");
					post=aux+"\n";
					hilo_encontrado=true;
				}
				else if(aux.equals("</document></vespaadd>")&& almacena){
					post = post.replaceAll("<document type=\"wisdom\">", "<document>");
					post = post.replaceAll("</vespaadd>", "");
					
					hilo_encontrado = false;
					pw.print(post);
					almacena=false;
					contador++;
					continue;
				}
				if(hilo_encontrado && aux.startsWith("<maincat>"+mainccat))
					almacena=true;
			}
			pw.println("</feeds>"
					+ ""
					+ ""
					+ ">");
			pw.close ();
			fichero.close();
			System.out.println("Fin !: "+stopTimeCount()+" ms");
			System.out.println("Total registrados: " + contador);
		}

		catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		catch (Exception e1) {
			e1.printStackTrace();
		}

	}
}
