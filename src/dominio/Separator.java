/**
 * 
 */
package dominio;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * @author Sergio Fern�ndez
 *
 */
public class Separator {


	
	//Medida de los tiempos
	static long time_start = 0;
	static long time_end = 0;


	private static void startTimeCount() {
		time_start = System.currentTimeMillis();
	}

	private static long stopTimeCount() {
		time_end = System.currentTimeMillis();
		return time_end - time_start;
	}

	/**
	 * @param args
	 */
	public Separator() {
	}

	public void dividirFichero(String categoria, int factor, boolean are_pieces) {

		// /Inicializacion de variables de acceso a fichero
		File fich_fuente, fich_parte;
		
		FileReader fr;
		BufferedReader br;

		FileWriter fw = null;		
		PrintWriter pw = null;
		Scanner fichero;


		//Axuliares
		String aux = "";
		String post="";
		double fich_size =0;
		double piece_size = 0;
		int n_pieces = 0;

		boolean go = false;


		// Acceso a fichero
		try {
			fich_fuente = new File(categoria+".xml");
			fich_size = fich_fuente.length()/(Math.pow(1024, 2));
			System.out.println("Tama�o del fichero: " + fich_size +" MB");
			
			if(are_pieces){
				n_pieces = factor;
				piece_size = (double)(fich_size/factor);
			}
			else{
				n_pieces = (int) Math.ceil(fich_size/factor);
				piece_size = factor;
			}
			fr = new FileReader(fich_fuente);
			br = new BufferedReader(fr);
			fichero = new Scanner(br);

			//Creaci�n de cada uno de las partes del fichero

			startTimeCount();
			
			fichero.nextLine();
			fichero.nextLine();
			
			for(int i = 0; i<n_pieces; i++){
				fich_parte = new File("secciones/" + categoria + "_" + i + ".xml");
				fw = new FileWriter(fich_parte);
				pw = new PrintWriter(fw);
				pw.println("<?xml version='1.0' encoding='UTF-8'?>\n<feeds>");
				go=true;
				post="";
				while(fichero.hasNextLine() && go){
					aux = fichero.nextLine();
					post+=aux+"\n";
					
					if(aux.equals("</document>")){
						pw.print(post);
						//Se comprueba el tama�o del fichero.
						post="";
						go = ((fich_parte.length()/Math.pow(1024, 2)) >= piece_size) ? false : true;
					}					
				}
				pw.println("</feeds>");
				fw.close();
				pw.close ();
			}
			
			fichero.close();
			System.out.println("Fin !: "+stopTimeCount()+" ms");
		}

		catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		catch (Exception e1) {
			e1.printStackTrace();
		}

	}
}
