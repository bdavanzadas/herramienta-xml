/**
 * 
 */
package dominio;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author Sergio Fernández
 *
 */
public class Joiner {

	
	//Medida de los tiempos
	static long time_start = 0;
	static long time_end = 0;


	private static void startTimeCount() {
		time_start = System.currentTimeMillis();
	}

	private static long stopTimeCount() {
		time_end = System.currentTimeMillis();
		return time_end - time_start;
	}

	/**
	 * @param args
	 */
	public Joiner() {
	}

	public void unirFicheros(String categoria, ArrayList<String> lista_etiquetas) {
		// /Inicializacion de variables de acceso a fichero
		//File ini;
		
		File fich_destino, fich_parte;
		
		FileReader fr;
		BufferedReader br;

		FileWriter fw = null;		
		PrintWriter pw = null;
		Scanner fichero;

		//Axuliares
		String aux = "";

		boolean copiar = false;
		int n_etiquetas = lista_etiquetas.size();
		long contador = 0; 
		String cierre = "";


		// Acceso a fichero
		try {
			fich_destino = new File("uniones/" + categoria + ".xml");
			fich_parte = new File("secciones/" + categoria + "_"+ contador + ".xml");
			
			
			fr = new FileReader(fich_parte);
			br = new BufferedReader(fr);
			
			fichero = new Scanner(br);

			fw = new FileWriter(fich_destino);
			pw = new PrintWriter(fw);

			if(fich_parte.exists()){
				startTimeCount();
				
				System.out.println(fich_parte.getName());
				
				//Para el primer archivo a unir
				while (fichero.hasNextLine()) {
					aux = fichero.nextLine();
					
					if(fichero.hasNextLine()){
						pw.println(aux);
					}
					else{
						cierre = aux;
					}
				}
				fichero.close();
				
				
				//Para el resto de ficheros
				contador++;
				fich_parte = new File("secciones/" + categoria + "_"+ contador + ".xml");
				
				while(fich_parte.exists()){
					
					System.out.println(fich_parte.getName());
					
					fr = new FileReader(fich_parte);
					br = new BufferedReader(fr);
					fichero = new Scanner(br);
						
					if(fich_parte.exists()){
						while (fichero.hasNextLine()) {
							copiar = true;
							aux = fichero.nextLine();
							
							for(int i=0; i < n_etiquetas; i++){
								if(aux.contains(lista_etiquetas.get(i))){
									copiar = false;
									break;
								}
							}
							
							if(fichero.hasNextLine() && copiar){
								pw.println(aux);
							}else{
								cierre = aux;
							}						
						}
						
						fichero.close();
						
						contador++;
						fich_parte = new File("secciones/" + categoria + "_"+ contador + ".xml");
					}
				}
				
				pw.write(cierre);
				pw.close ();
				fichero.close();
			}
			
			
			
			System.out.println("Fin !: "+stopTimeCount()+" ms");
			System.out.println("Total ficheros: " + contador);
		}

		catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		catch (Exception e1) {
			e1.printStackTrace();
		}

	}
}
