/**
 * 
 */

import java.util.ArrayList;
import java.util.Scanner;

import dominio.Extractor;
import dominio.Joiner;
import dominio.Separator;

/**
 * @author Sergio Fern�ndez
 *
 */
public class Main {
	private static final String FICHERO_XML = "FullOct2007.xml";
	private static Scanner leer = new Scanner(System.in);
	

	public static void main(String [] args){
		
		
		int operacion = -1;

		while (operacion!=0){
			menuOpciones();
			operacion = leer.nextInt();

			switch (operacion){
			case 0:
				break;

			case 1: //Men� para la parte de extracci�n.
				extraccion();
				break;

			case 2: //Men� para partir ficheros demasiado grandes.
				division();
				break;

			case 3: //Men� para unir ficheros (concatenar salvando las cabeceras de los ficheros)
				union();
				break;

			default:
				System.out.println("\t[!] Opci�n incorrecta, seleccione entre [0- 3] [!]");
				operacion= -1;
				leer.nextLine();
				break;
			}
		}
	}

	private static void extraccion() {
		int opcion = -1;
		String fichero_xml = null;
		ArrayList<String> list_categorias = new ArrayList<String>();
		String categoria = null;
		
		while (opcion!=0){
			leer.nextLine();
			menuExtraccion();
			opcion = leer.nextInt();
			switch (opcion){
			case 0:
				break;

			case 1: // Se selecciona el fichero del cual se obtienen los datos.
				leer.nextLine();
				System.out.println("Introduzca el nombre del fichero: ");
				fichero_xml = leer.nextLine();
				
				if(fichero_xml.equals("") || fichero_xml.equals(null)){
					
					fichero_xml = FICHERO_XML;
				}
				
				System.out.println("\t [*] El fichero se ha establecido como: " + fichero_xml + " [*]\n");
				break;

			case 2: // Se seleccionan las categor�as que se han de consultar el el fichero del paso 1.
				//Si no se ha seleccionado el fichero se da aviso y no se procede a la introducci�n de categor�as.
				try{
					if(!fichero_xml.equals(null)){
						System.out.println("Escriba las categor�as pulsando intro.\n"
								+ "Para terminar de introducir categor�as pulse intro sin introducir texto.");
						list_categorias.clear();
						
						leer.nextLine();
						categoria = leer.nextLine();
						
						while(!categoria.equals("")){ 
							if(!list_categorias.contains(categoria))
								list_categorias.add(categoria);
							else
								System.out.println("La categor�a "+ categoria + " ya ha sido introducida con anterioridad.");
							
							categoria = leer.nextLine();
						}
					}
				}catch (NullPointerException e) {
					System.out.println("\t[!] Hasta que no se defina el fichero base en el paso 1\n"
							+ "\t  no podr� definir las categor�as. [!]");
				}
				break;

			case 3: // Comenzar� la extracci�n de las diferentes categor�as. 
				try{
					if((!fichero_xml.equals(null)) && (list_categorias != null)){
						extraer(fichero_xml, list_categorias);
					}
					
					if(list_categorias.size() < 1){
						System.out.println("\t[!]Debe realizar el paso 2 para poder realizar esta operaci�n.[!]");
					}
					
				}catch (NullPointerException e) {
					System.out.println("\t[!]Debe realizar los pasos 1 y 2 para poder realizar esta operaci�n.[!]");
				}
				break;

			default:
				System.out.println("\t[!] Opci�n incorrecta, seleccione entre [0- 3] [!]");
				opcion= -1;

			}	
		}
	}

	private static void division(){
		int opcion = -1;
		ArrayList<String> list_categorias = new ArrayList<String>();
		ArrayList<Integer> list_size = new ArrayList<Integer>();
		ArrayList<Integer> list_pieces = new ArrayList<Integer>();
		String categoria = null;
		int size = -1;
		int i = 0;
		int n_categorias = 0;
		
		while (opcion!=0){
			leer.nextLine();
			menuPartir();
			opcion = leer.nextInt();
			switch (opcion){
			case 0:
				break;

			case 1: // Se seleccionan la categor�a a unir.

				System.out.println("Escriba las categor�as a dividir pulsando intro.\n"
						+ "Para terminar de introducir categorias pulse intro sin introducir texto.");
				list_categorias.clear();
				
				leer.nextLine();
				categoria = leer.nextLine();
				
				while(!categoria.equals("")){ 
					if(!list_categorias.contains(categoria))
						list_categorias.add(categoria);
					else
						System.out.println("La categor�a "+ categoria + " ya ha sido introducida con anterioridad.");
					
					categoria = leer.nextLine();
				}
				break;

			case 2: // Seleccionar tama�o aproximado de cada parte (MB) para cada fichero..
				//Si no se ha seleccionado la categor�a se da aviso y no se procede a la introducci�n de categor�as.
				n_categorias = list_categorias.size();
				try{
					if(!list_categorias.equals(null)){
						System.out.println("Escriba los tama�os asociados a cada categor�a pulsando intro.\n");
						list_size.clear();
						leer.nextLine();
						
						for(i=0; i< n_categorias; i++){
							System.out.print("Tama�o (MB) para la categor�a "+ list_categorias.get(i)+": ");
							size = leer.nextInt();
							list_size.add(size);
						}
					}
				}catch (NullPointerException e) {
					System.out.println("\t[!] Hasta que no se defina las categorias en el paso 1\n"
							+ "\t  no podr� definir el tama�o de cada parte y categor�a. [!]");
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				
				break;

			case 3: // Seleccionar numero de divisiones que se desean de cada categor�a.
				//Si no se ha seleccionado la categor�a se da aviso y no se procede a la introducci�n de categor�as.
				n_categorias = list_categorias.size();
				try{
					if(!list_categorias.equals(null)){
						System.out.println("Escriba el n�mero de dvisiones asociadas a cada categor�a pulsando intro.\n");
						list_pieces.clear();
						leer.nextLine();
						
						for(i=0; i< n_categorias; i++){
							System.out.print("Divisiones para la categor�a "+ list_categorias.get(i)+": ");
							size = leer.nextInt();
							list_pieces.add(size);
						}
					}
				}catch (NullPointerException e) {
					System.out.println("\t[!] Hasta que no se defina las categorias en el paso 1\n"
							+ "\t  no podr� definir el tama�o de cada parte y categor�a. [!]");
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				break;
				
			case 4: // Comenzar� la divisi�n de los diferentes archivos. 
				try{
					if((list_categorias != null) && ((list_size != null) || (list_pieces != null))){
						patrir(list_categorias, list_size, list_pieces);
					}
					
					if(list_categorias.size() < 1){
						System.out.println("\t[!]Debe realizar el paso 2 o 3 para poder realizar esta operaci�n.[!]");
					}
					
				}catch (NullPointerException e) {
					System.out.println("\t[!]Debe realizar los pasos 1 y (2 o 3) para poder realizar esta operaci�n.[!]");
				}
				break;

			default:
				System.out.println("\t[!] Opci�n incorrecta, seleccione entre [0- 4] [!]");
				opcion= -1;
			}	
		}
	}
	
	private static void union(){
		int opcion = -1;
		String categoria_union = null;
		ArrayList<String> list_etiquetas = new ArrayList<String>();
		String etiqueta = null;
		
		while (opcion!=0){
			leer.nextLine();
			menuUnir();
			opcion = leer.nextInt();
			switch (opcion){
			case 0:
				break;

			case 1: // Se seleccionan la categor�a a unir.
				leer.nextLine();
				System.out.println("Introduzca el nombre de la categor�a: ");
				categoria_union = leer.nextLine();
				if(categoria_union.equals("") || categoria_union.equals(null)){
					categoria_union = null;
					System.out.println("\t[!] No se ha reconocido la categoria. [!]");
				}else{
					System.out.println("\t[*] La categor�a se ha establecido como: " + categoria_union + " [*]\n");	
				}
				break;

			case 2: // Se seleccionan las etiquetas que se han de suprimir a la hora de copiar.
				//Si no se ha seleccionado la categor�a se da aviso y no se procede a la introducci�n de categor�as.
				try{
					if(!categoria_union.equals(null)){
						System.out.println("Escriba las etiquetas a suprimir pulsando intro.\n"
								+ "Para terminar de introducir etiquetas pulse intro sin introducir texto.");
						list_etiquetas.clear();
						
						leer.nextLine();
						etiqueta = leer.nextLine();
						
						while(!etiqueta.equals("")){ 
							if(!list_etiquetas.contains(etiqueta))
								list_etiquetas.add(etiqueta);
							else
								System.out.println("La etiqueta "+ etiqueta + " ya ha sido introducida con anterioridad.");
							
							etiqueta = leer.nextLine();
						}
					}
				}catch (NullPointerException e) {
					System.out.println("\t[!] Hasta que no se defina la categoria en el paso 1\n"
							+ "\t  no podr� definir las etiquetas a suprimir. [!]");
				}
				break;

			case 3: // Comenzar� la uni�n de los diferentes archivos. 
				try{
					if((!categoria_union.equals(null)) && (list_etiquetas != null)){
						unir(categoria_union, list_etiquetas);
					}
					
					if(list_etiquetas.size() < 1){
						System.out.println("\t[!]Debe realizar el paso 2 para poder realizar esta operaci�n.[!]");
					}
					
				}catch (NullPointerException e) {
					System.out.println("\t[!]Debe realizar los pasos 1 y 2 para poder realizar esta operaci�n.[!]");
				}
				break;

			default:
				System.out.println("\t[!] Opci�n incorrecta, seleccione entre [0- 3] [!]");
				opcion= -1;
			}	
		}
	}

	private static void menuUnir() {
		System.out.println("\n\t\t===[ MEN� UNI�N ]===");
		System.out.println("1.- Seleccionar la categor�a.");
		System.out.println("2.- Tags �nicas que aparecer�n una sola vez (Apertura/cierre).");
		System.out.println("3.- Unir ficheros.");
		System.out.println("");
		System.out.println("0.- Salir");
		System.out.println("");
		System.out.print("  [*] Elija una opci�n [0-3]: ");
		
	}

	private static void menuPartir() {
		System.out.println("\n\t\t===[ MEN� PARTICI�N ]===");
		System.out.println("1.- Seleccionar ficheros que ser�n divididos.");
		System.out.println("2.- Seleccionar tama�o aproximado de cada parte (MB) para cada fichero.");
		System.out.println("3.- Seleccionar el numero de partes para cada fichero en las que se desea partir.");
		System.out.println("4.- Partir ficheros.");
		System.out.println("");
		System.out.println("0.- Salir");
		System.out.println("");
		System.out.print("  [*] Elija una opci�n [0-4]: ");
	}

	private static void menuExtraccion() {
		System.out.println("\n\t\t===[ MEN� EXTRACCI�N ]===");
		System.out.println("1.- Seleccionar fichero del cual extraer.");
		System.out.println("2.- Seleccionar categor�as.");
		System.out.println("3.- Extraer categor�as del fichero.");
		System.out.println("");
		System.out.println("0.- Salir");
		System.out.println("");
		System.out.print("  [*] Elija una opci�n [0-3]: ");		
	}

	private static void menuOpciones(){
		System.out.println("\n\t\t===[ MEN� PRINCIPAL ]===");
		System.out.println("1.- Extracci�n de las categor�as.");
		System.out.println("2.- Partir documentos demasiado grandes.");
		System.out.println("3.- Unir ficheros.");
		System.out.println("");
		System.out.println("0.- Salir");
		System.out.println("");
		System.out.print("  [*] Elija una opci�n [0-3]: ");
	}

	private static void extraer(String fichero_xml, ArrayList<String> listado_categorias){
		Extractor extractor = new Extractor();
		int i = 0;
		int n_categorias = listado_categorias.size();
		for(i=0; i< n_categorias; i++){
			extractor.extraerCategoriaDe(listado_categorias.get(i), fichero_xml);   
		}
	}
	
	private static void unir(String categoria_unir, ArrayList<String> listado_etiquetas){
		Joiner joiner = new Joiner();
		joiner.unirFicheros(categoria_unir, listado_etiquetas);   
	}
	
	private static void patrir(ArrayList<String> listado_categorias, ArrayList<Integer> listado_size,
				ArrayList<Integer> listado_pieces){
		
		Separator separator = new Separator();
		boolean pieces = (listado_pieces.size()>0);
		
		int i = 0;
		int n_categorias = listado_categorias.size();
		if(pieces){
			for(i=0; i< n_categorias; i++){
				separator.dividirFichero(listado_categorias.get(i), listado_pieces.get(i), pieces);   
			}
		}
		else{
			for(i=0; i< n_categorias; i++){
				separator.dividirFichero(listado_categorias.get(i), listado_size.get(i),pieces);   
			}			
		}
	}
}	


