<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<link rel="stylesheet" type="text/css" href="style.css"/>
		<html>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
			
			<body>
				<header>
					<div><img src="imagenYahoo.png" alt=""/></div>
				</header>
				
				<nav>
					<ul>
					<p> Sergio Fernandez Garcia - Pilar Garcia Martin de la Puente</p>
					</ul>
				</nav>
				
				<div style="float: left;">
				
					<footer><p>Yahoo Answers. </p><p>Trabajo para Bases de Datos Documentales</p></footer>
					
					<div id="center" class="column">
						<h2>Contenido de esta pagina</h2>
						<ul>
							<p>Yahoo! Respuestas es un servicio de Internet que le permite a sus usuarios tanto formular preguntas como responderlas. Tanto las preguntas como las respuestas son públicas por lo que cualquier usuario, registrado o no, podrá consultar, navegary bajar estos contenidos.</p>
							<p>Este es el resultado de la selección y muestra de las categorias de "HEALTH" y "DINING OUT" de los datos obtenidos de FullOct2007.xml</p>
						 </ul>
					 </div>
					 
					 <div><p>.</p></div>
					 <div class="container">
						 <table style="width:100%" class="tabla" border="5px">
							 <tr bgcolor="#c2f7d3">
								 <th class="row-1 columna_pregunta"> PREGUNTA </th><th class="row-2 columna_tema"> TEMA </th>
								 <th class="row-3 columna_contenido"> CONTENIDO </th>
								 <th class="row-4 columna_respuesta"> MEJOR RESPUESTA </th>
							 </tr>
							 
							 <xsl:for-each select="feeds/document">
                                    <tr>
                                        <td><xsl:value-of select="subject"/></td>
                                        <td><xsl:value-of select="subcat"/></td>
                                        <td><xsl:value-of select="content"/></td>
                                        <td><xsl:value-of select="bestanswer"/></td>
                                    </tr>
                                </xsl:for-each>
						</table>
					</div>
				</div>
			</body>
		</html>
	</xsl:template>

</xsl:stylesheet>