<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<html> 
<body>
  <div><img src="imagenYahoo.png" alt=""/></div>
  <h2>Resultados preguntas</h2>
  <table border="1">
    <tr bgcolor="#c2f7d3">
      <th style="text-align:left">Pregunta</th>
      <th style="text-align:left">Tema</th>
      <th style="text-align:left">Contenido</th>
      <th style="text-align:left">Mejor respuesta</th>
    </tr>
    <xsl:for-each select="ystfeed/vespaadd/document">
    <tr>
      <td><xsl:value-of select="subject"/></td>
      <td><xsl:value-of select="subcat"/></td>
      <td><xsl:value-of select="content"/></td>
      <td><xsl:value-of select="bestanswer"/></td>
    </tr>
    </xsl:for-each>
  </table>
</body>
</html>
</xsl:template>
</xsl:stylesheet>
